import mongoose from 'mongoose';
import {Router} from 'express';
import Restaurant from '../model/restaurant';
import bodyParser from 'body-parser';
import Review from '../model/review';
import { authenticate } from '../middleware/authMiddleware'

export default({config, db}) => {
    let api = Router();

    // CRUD - Create, Read, Update, Delete
    // index.js adds v1 > routes than adds /restaurant and below we add /add
    // and that is how we get the full url 'v1/restaurant/add' -- Create Method
    // api.post has a function which return res and req

    api.post('/add', authenticate, (req, res) => {
        // created a new instance of restaurant model
        let newRest = new Restaurant();
        // setting the name vaule in model
        newRest.name = req.body.name;
        // setting the foodtype in model
        newRest.foodtype = req.body.foodtype;
        // setting the avgcost in model
        newRest.avgcost = req.body.avgcost;
        // setting the geometry coordinates
        newRest.geometry.coordinates = req.body.geometry.coordinates;
        // saving the model to database
        newRest.save(err => {
            // checking for errors
            if (err) {
                // if error send the error back to client
                res.send(err);
            }
            // else send successful message to client
            res.json({message: 'Restaurant saved successfully'});
        });
    });

    // 'v1/restaurant' -- Read method

    api.get('/', (req, res) => {
        // getting mongoose to find all the restaurant by supplying empty curly braces
        // will either return error or list of restaurant
        Restaurant.find({}, (err, restaurants) => {
            // check if its an error
            if (err) {
                // send error back to client
                res.send(err);
            }
            if (restaurants.length === 0) {
                res.json({success: 0, message: 'No restaurants found with that id'});
            } else {
                // else send restaurant to client
                res.json({success: 1, message: 'success', restaurant: restaurants});
            }
        });
    });

    // 'v1/restaurant/:id' - Read, will only get a restaurant by id
    api.get('/:id', (req, res) => {
        // using findById and passing the signle id coming from client to get data
        Restaurant.findById(req.params.id, (err, restaurant) => {
            // checking if there is error
            if (err) {
                // if error found send the error back to client
                res.send(err);
            }
            if (restaurant.length === 0) {
                res.json({success: 0, message: 'No restaurants found with that id'});
            } else {
                // else send restaurant
                res.json({success: 1, message: 'success', restaurant: restaurant});
            }
        });
    });

    // 'v1/restaurant/:name' - Read will only get a restaurant by name
    api.get('/name/:name', (req, res) => {
        // pass the name parameter, also making the search non case sensitive
        // returns either err or restaurant
        Restaurant.find({
            name: new RegExp(req.params.name, "i")
        }, (err, restaurant) => {
            // if error send the error
            if (err) {
                res.send(err);
            }
            if (restaurant.length === 0) {
                res.json({success: 0, message: 'No restaurants found with that name'});
            } else {
                // else return restaurant
                res.json({success: 1, message: 'success', restaurant: restaurant});
            }
        });
    });

    // 'v1/restaurant/:id' - Update
    // we pass the id, trys to find a restaurant if found update the property

    api.put('/:id', (req, res) => {
        // find the restaurant by id
        Restaurant.findById(req.params.id, (err, restaurant) => {
            // check for errors
            if (err) {
                // if error found return the error
                res.send(err);
            }
            // check restaurant isn't empty
            if (restaurant.length === 0) {
                // if length = 0 send fail message
                res.json({success: 0, message: 'No restaurants found with that id'});
            } else {
                // else update the restaurant name provided from client
                restaurant.name = req.body.name;
                restaurant.foodtype = req.body.foodtype;
                restaurant.avgcost = req.body.avgcost;
                restaurant.geometry.coordinates = req.body.geometry.coordinates;
                // try to save
                restaurant.save(err => {
                    // check for error
                    if (err) {
                        // if error return it to client
                        res.send(err);
                    } else {
                        // else return the success message to client
                        res.json({success: 1, message: 'Restaurant information updated successfully'});
                    }
                });
            }
        });
    });

    // 'v1/restaurant/:id' - Delete
    // we pass the id, trys to find a restaurant if found delete the restaurant

    api.delete('/:id', (req, res) => {
        // call the remove function pass the id
        Restaurant.remove({
            _id: req.params.id
        }, (err, restaurant) => {
            // check for error
            if (err) {
                // if error send the error
                res.send(err);
            }
            // now we need to remove the review with the restaurant
            Review.remove({
              // pass the restaurant
              restaurant: req.params.id
            }, (err, review) => {
              // check for error, if error send error to client
              if (err) {
                res.send(err);
              }
            })
            // otherwise send the success message
            res.json({success: 1, message: 'Restaurant Successfully Removed'})
        });
    });

    // add review for specific restaurant id
    // 'v1/restaurant/reviews/add/:id'

    api.post('/reviews/add/:id', (req, res) => {
      // find restaurant by id, returns either error or restaurant
      Restaurant.findById(req.params.id, (err, restaurant) => {
        // check for error
        if (err) {
          // if error send the error to client
          res.send(err);
        }
        // create a new instance of review
        let newReview = new Review();
        // setting the title with title passed from client
        newReview.title = req.body.title;
        // setting the text with text passed from client
        newReview.text = req.body.text;
        // setting the restaurant details with restaurant id passed from client
        newReview.restaurant = restaurant._id;
        // saving the review return error or review
        newReview.save((err, review) => {
          // if error send the error to client
          if (err) {
            res.send(err);
          }
          // else push the review to array of reviews for particular restaurant
          restaurant.reviews.push(newReview);
          // save the review to restaurant, check for error
          restaurant.save(err => {
            if (err) {
              // if error send the error to client
              res.send(err);
            }
            // else send successful message
            res.json({success: 1, message: 'Restaurant Review Successfully Saved'})
          });
        });
      });
    });

    // get reviews for a specific restaurant id
    // 'v1/restaurant/reviews/:id'

    api.get('/reviews/:id', (req, res) => {
      // get the reviews with passing the restaurant id
      Review.find({restaurant: req.params.id},(err, reviews) => {
        // check for errors
        if (err){
          // if error send the error to client
          res.send(err);
        }
        // else return reviews
        res.json({success: 1, message: 'success', reviews: reviews})
      });
    });


    return api;
}
