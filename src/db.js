import mongoose from 'mongoose';
import config from './config';

// when this is imported we will connect to database and
// pass the db back to whatever has called it
export default callback => {
    let db = mongoose.connect(config.mongoUrl);
    callback(db);
}
